const serverUrl = '//site-crawler-backend.xinshangshangxin.com';
export const environment = {
  production: true,
  serverUrl,
  apiUrl: `${serverUrl}/api/v1`,
  proxyUrl: '//site-crawler-backend.xinshangshangxin.com/api/v1/crawler/stream',
};
