import { Component, OnInit } from '@angular/core';
import { combineLatest, of } from 'rxjs';
import { catchError, debounceTime, distinctUntilChanged, startWith, switchMap, tap } from 'rxjs/operators';

import { Article, ArticleService, InfiniteArticleParams } from '../services/article.service';

@Component({
  selector: 'app-article-list',
  templateUrl: './article-list.component.html',
  styleUrls: ['./article-list.component.scss'],
})
export class ArticleListComponent implements OnInit {
  articles: Article[] = [];

  private infiniteArticleParams: InfiniteArticleParams = {
    sites: [],
    page: 1,
    search: '',
  };

  constructor(private articleService: ArticleService) {
    let searchSubject = this.articleService.searchSubject.pipe(
      startWith(''),
      debounceTime(300),
      distinctUntilChanged(),
      tap(() => {
        this.infiniteArticleParams.page = 1;
      })
    );

    let sitesSubject = this.articleService.sitesSubject.pipe(
      startWith([]),
      distinctUntilChanged((p1, p2) => {
        return p1.join(',') === p2.join(',');
      }),
      tap(() => {
        this.infiniteArticleParams.page = 1;
      })
    );

    let scrollSubject = this.articleService.scrollSubject.pipe(
      tap((reload: boolean) => {
        if (reload) {
          this.infiniteArticleParams.page = 1;
        }
      })
    );

    combineLatest(scrollSubject, searchSubject, sitesSubject)
      .pipe(
        tap((params) => {
          console.info('combineLatest params: ', params, this.infiniteArticleParams.page);
        }),
        debounceTime(300),
        switchMap(([reload, search, sites]) => {
          return of({
            reload: !!reload,
            page: this.infiniteArticleParams.page,
            search: search,
            sites: sites,
          });
        }),
        distinctUntilChanged<InfiniteArticleParams>((p1, p2) => {
          if (p2.reload) {
            return false;
          }

          return (
            p1.page === p2.page &&
            p1.search === p2.search &&
            p1.sites.join(',') === p2.sites.join(',')
          );
        }),
        tap((params: InfiniteArticleParams) => {
          console.info('getArticles params: ', params);
        }),
        switchMap((params: InfiniteArticleParams) => {
          if (!params.sites || !params.sites.length) {
            return of([]);
          }

          return this.articleService.getArticles({
            sites: params.sites,
            search: params.search,
            page: params.page,
          });
        }),
        catchError((e, caught) => {
          console.warn(e);
          return caught;
        })
      )
      .subscribe((articles: Array<Article>) => {
        console.info('articles: ', articles);
        if (this.infiniteArticleParams.page === 1) {
          this.articles = [];
        }
        this.articles.push(...articles);
      });
  }

  ngOnInit() {
    this.articleService.scrollSubject.next(true);
  }

  onScroll() {
    console.log('scrolled!!');
    this.infiniteArticleParams.page += 1;
    this.articleService.scrollSubject.next();
  }
}
