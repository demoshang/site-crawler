import { Pipe, PipeTransform } from '@angular/core';
import { environment } from '../../environments/environment';

@Pipe({
  name: 'proxy',
})
export class ProxyPipe implements PipeTransform {
  transform(url: string): string {
    if (/data:image/.test(url)) {
      return url;
    }

    return environment.proxyUrl + '?url=' + encodeURIComponent(url);
  }
}
