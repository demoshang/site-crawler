import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import { Article } from './article.service';
import { StorageService } from './storage.service';

interface Selector {
  delay: number;
  multiple: boolean;
  parentSelectors: string[];
  selector: string;
  type: string;
  id: string;
}

interface Sitemap {
  selectors: Selector[];
  startUrl: string[];
  _id: string;
}

interface RequestOption {
  url: string;
}

interface Img {
  fn: string;
  params: string[];
}

interface Date {
  fn: string;
  param: string;
}

interface Transform {
  img: Img;
  date: Date;
}

interface NextPageRequestOption {
  url: string;
}

export interface Site {
  site: string;
  createdAt: string;
  sitemap: Sitemap;
  requestOptions: RequestOption;
  href: string;
  description: string;
  updatedAt: string;
  transform: Transform;
  nextPageRequestOptions: NextPageRequestOption;
  isCrawler: boolean;
  isShowArticle: boolean;
  latestCrawlerTime?: string;
  _id: string;
}

export interface UpdateTime {
  updateTime: string;
}

export interface TaskUpdate {
  start: boolean;
}

export interface LatestCrawlerTime {
  _id: string;
  updatedAt: string;
  createdAt: string;
  site: string;
}

@Injectable()
export class SiteService {
  private sitesSaveKey = 'localSites';

  constructor(private http: HttpClient, private storageService: StorageService) {}

  private differenceBy(arr1, arr2, key) {
    if (arr1.length === 0) {
      return arr2;
    }
    if (arr2.length === 0) {
      return arr1;
    }

    return arr1.filter((item1) => {
      let index = arr2.findIndex((item2) => {
        return item1[key] === item2[key];
      });

      return index === -1;
    });
  }

  getSites(): Observable<Site[]> {
    return this.http.get<Site[]>(`${environment.apiUrl}/crawler-rule`);
  }

  getSite(id: string): Observable<Site> {
    return this.http.get<Site>(`${environment.apiUrl}/crawler-rule/${id}`);
  }

  updateSite(doc: Site): Observable<Site> {
    return this.http.post<Site>(`${environment.apiUrl}/crawler-rule`, doc);
  }

  getUpdateTime(): Observable<UpdateTime> {
    return this.http.get<UpdateTime>(`${environment.apiUrl}/record/update-time`);
  }

  taskUpdate(): Observable<TaskUpdate> {
    return this.http.get<TaskUpdate>(`${environment.apiUrl}/task/update-site`);
  }

  getExportUrl(): string {
    return `${environment.apiUrl}/crawler-rule/export`;
  }

  import(obj: object): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl}/crawler-rule/import`, obj);
  }

  delete(id: string): Observable<any> {
    return this.http.delete<any>(`${environment.apiUrl}/crawler-rule/${id}`);
  }

  preview(body: Site) {
    return this.http.post<Article[]>(`${environment.apiUrl}/crawler-rule/preview`, body);
  }

  latestCrawlerTime(site: string) {
    return this.http.get<LatestCrawlerTime>(`${environment.apiUrl}/record/${site}`);
  }

  mergeSites(remoteSites, localSites) {
    let deletedSites = this.differenceBy(localSites, remoteSites, 'site');
    let newSites = this.differenceBy(remoteSites, localSites, 'site');

    deletedSites.forEach((deletedSite) => {
      let index = localSites.findIndex((item) => {
        return item.site === deletedSite.site;
      });

      localSites.splice(index, 1);
    });

    newSites.forEach((newSite) => {
      let index = remoteSites.findIndex((item) => {
        return item.site === newSite.site;
      });

      localSites.push(remoteSites[index]);
    });

    let isValid = false;
    for (const item of localSites) {
      if (item && item.isShowArticle) {
        isValid = true;
        break;
      }
    }

    if (!isValid) {
      let sites = remoteSites.map((item) => {
        return {
          site: item.site,
          isShowArticle: item.isShowArticle,
        };
      });

      this.storageService.setItem(this.sitesSaveKey, sites);
      return sites;
    }

    console.info('deletedSites: ', deletedSites);
    console.info('newSites: ', newSites);
    if (deletedSites.length || newSites.length) {
      let temp = localSites.map((item) => {
        return {
          site: item.site,
          isShowArticle: item.isShowArticle,
        };
      });

      this.storageService.setItem(this.sitesSaveKey, temp);
      return temp;
    }

    return localSites;
  }

  updateLocalSite() {
    return this.getSites().pipe(
      switchMap((sites: Site[]) => {
        let localSites = [];
        try {
          localSites = JSON.parse(localStorage.getItem(this.sitesSaveKey)) || [];
        } catch (e) {
          console.warn(e);
        }

        let siteList = this.mergeSites(sites, localSites);
        return of(siteList);
      })
    );
  }

  saveLocalSites(sites: any) {
    this.storageService.setItem(this.sitesSaveKey, sites);
  }
}
