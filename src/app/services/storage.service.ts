import { Injectable } from '@angular/core';

@Injectable()
export class StorageService {
  constructor() {}

  setItem(key: string, value: any) {
    let str = JSON.stringify(value);
    localStorage.setItem(key, str);
  }

  assign(key: string, value: object) {
    let origin = this.getItem(key);
    if (origin === null || origin === undefined) {
      origin = {};
    } 
    else if (typeof origin !== 'object') {
      throw new Error('origin saved not json');
    }

    Object.assign(origin, value);

    localStorage.setItem(key, JSON.stringify(value));
  }

  getItem(key) {
    let origin = localStorage.getItem(key);

    let value;
    try {
      value = JSON.parse(origin);
    } catch (e) {
      value = origin;
    }

    return value;
  }
}
