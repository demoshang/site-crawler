import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { map, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';

export interface Article {
  _id: string;
  href: string;
  updatedAt: string;
  date: string;
  title: string;
  gatherTime: string;
  img: string;
  time: string;
  intro: string;
  site: string;
  createdAt: string;
  id: string;
}

export interface InfiniteArticleParams {
  page: number;
  reload?: boolean;
  sites?: Array<string>;
  search?: string;
}

@Injectable()
export class ArticleService {
  constructor(private http: HttpClient) {}

  searchSubject = new Subject<string>();
  sitesSubject = new Subject<Array<string>>();
  scrollSubject = new Subject<boolean>();

  getArticles({ sites, search = '', page = 1, limit = 10 }): Observable<Article[]> {
    let params = new HttpParams({
      fromObject: {
        page: `${page}`,
        limit: `${limit}`,
        search,
        sites,
      },
    });

    return this.http.get<Article[]>(`${environment.apiUrl}/article`, {
      params,
    });
  }
}
