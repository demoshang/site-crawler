import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgProgressModule } from '@ngx-progressbar/core';
import { NgProgressHttpModule } from '@ngx-progressbar/http';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ArticleListComponent } from './article-list/article-list.component';
import { ArticlePreviewComponent } from './article-preview/article-preview.component';
import { HeaderSearchComponent } from './header/header-search/header-search.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { MyCustomMaterialModule } from './my-customer-material';
import { ArticleService } from './services/article.service';
import { SiteService } from './services/site.service';
import { StorageService } from './services/storage.service';
import { SiteDetailComponent } from './site-detail/site-detail.component';
import { SiteListComponent } from './site-list/site-list.component';
import { ProxyPipe } from './pipes/proxy.pipe';

@NgModule({
  declarations: [
    AppComponent,
    ArticlePreviewComponent,
    HeaderComponent,
    HomeComponent,
    HeaderSearchComponent,
    ArticleListComponent,
    SiteListComponent,
    SiteDetailComponent,
    ProxyPipe,
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    MyCustomMaterialModule,
    HttpClientModule,
    NgProgressModule.forRoot(),
    NgProgressHttpModule,
    InfiniteScrollModule,
  ],
  providers: [SiteService, ArticleService, StorageService],
  bootstrap: [AppComponent],
  entryComponents: [ArticlePreviewComponent],
})
export class AppModule {}
