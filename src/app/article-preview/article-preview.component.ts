import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';

import { Article } from '../services/article.service';

@Component({
  selector: 'app-article-preview',
  templateUrl: './article-preview.component.html',
  styleUrls: ['../article-list/article-list.component.scss'],
})
export class ArticlePreviewComponent implements OnInit {
  constructor(@Inject(MAT_DIALOG_DATA) public articles: Article[]) {}

  ngOnInit() {}
}
