import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatSnackBar } from '@angular/material';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { of as observableOf } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { ArticlePreviewComponent } from '../article-preview/article-preview.component';
import { Site, SiteService } from '../services/site.service';

@Component({
  selector: 'app-site-detail',
  templateUrl: './site-detail.component.html',
  styleUrls: ['./site-detail.component.scss'],
})
export class SiteDetailComponent implements OnInit {
  siteId: String;
  formGroup: FormGroup;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private siteService: SiteService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog
  ) {}

  ngOnInit() {
    let fb = new FormBuilder();

    this.formGroup = fb.group({
      _id: ['', [Validators.required]],
      site: ['', [Validators.required, Validators.minLength(2)]],
      description: ['', [Validators.required]],
      href: ['', [Validators.required]],
      requestOptions: [undefined, [Validators.required]],
      sitemap: [undefined, [Validators.required]],
      transform: [],
      nextPageRequestOptions: [],
      isShowArticle: [],
      isCrawler: [],
    });

    this.activatedRoute.params
      .pipe(
        switchMap((params: Params) => {
          if (params.id === 'add') {
            this.formGroup.removeControl('_id');
            return observableOf({} as Site);
          }

          return this.siteService.getSite(params.id);
        })
      )
      .subscribe(
        ({
          _id,
          site,
          sitemap,
          requestOptions,
          href,
          description,
          transform,
          nextPageRequestOptions,
          isCrawler,
          isShowArticle,
        }) => {
          this.formGroup.reset({
            _id,
            site,
            sitemap: sitemap ? JSON.stringify(sitemap, null, 2) : '',
            requestOptions: requestOptions ? JSON.stringify(requestOptions, null, 2) : '',
            href,
            description,

            transform: transform ? JSON.stringify(transform, null, 2) : '',
            nextPageRequestOptions: nextPageRequestOptions
              ? JSON.stringify(nextPageRequestOptions, null, 2)
              : '',
            isCrawler,
            isShowArticle,
          });
        }
      );
  }

  save() {
    console.info(this.formGroup.value);

    this.siteService.updateSite(this.formGroup.value).subscribe(
      () => {
        this.ngOnInit();
      },
      (err) => {
        console.info(err);
        this.snackBar.open('保存失败' + err.message, 'ok');
      }
    );
  }

  delete() {
    this.siteService.delete(this.formGroup.value._id).subscribe(
      () => {
        this.router.navigate(['/setting']);
      },
      (err) => {
        console.info(err);
        this.snackBar.open('删除失败' + err.message, 'ok');
      }
    );
  }

  preview() {
    let site = this.formGroup.value;
    this.siteService.preview(site).subscribe((articles) => {
      articles = articles.map((item) => {
        item.site = site.site;
        return item;
      });

      console.info('articles: ', articles);
      this.dialog.open(ArticlePreviewComponent, {
        data: articles,
        height: '500px',
      });
    });
  }
}
