import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  OnChanges,
  SimpleChanges,
} from '@angular/core';

import { ArticleService } from '../../services/article.service';

@Component({
  selector: 'app-header-search',
  templateUrl: './header-search.component.html',
  styleUrls: ['./header-search.component.scss'],
})
export class HeaderSearchComponent implements OnInit, OnChanges {
  searchValue: string = '';

  @Input()
  sites: string[];

  @Input()
  selectedSites: string[];

  @Output('selectedSitesChange')
  selectedSitesChange: EventEmitter<string[]> = new EventEmitter();

  constructor(private articleService: ArticleService) {}

  ngOnInit() {
    this.articleService.sitesSubject.next(this.selectedSites);
    this.articleService.searchSubject.next(this.searchValue);
  }

  ngOnChanges(changes: SimpleChanges) {
    this.articleService.sitesSubject.next(changes.selectedSites.currentValue);
  }

  selectionChange(e) {
    this.articleService.sitesSubject.next(e.value);
    this.selectedSitesChange.emit(e.value);
  }

  inputKeyup(e) {
    this.articleService.searchSubject.next(this.searchValue);
  }

  clear() {
    this.searchValue = '';
    this.articleService.searchSubject.next(this.searchValue);
  }
}
