import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';

import { ArticleService } from '../services/article.service';
import { SiteService } from '../services/site.service';
import { StorageService } from '../services/storage.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  sites = [];
  selectedSites = [];

  updateTime: string;

  constructor(
    private storageService: StorageService,
    private articleService: ArticleService,
    private siteService: SiteService,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit() {
    this.siteService.updateLocalSite().subscribe((arr) => {
      this.sites = arr.map((item) => {
        return item.site;
      });

      this.selectedSites = arr
        .filter((item) => {
          return item.isShowArticle;
        })
        .map((item) => {
          return item.site;
        });
    });

    this.siteService.getUpdateTime().subscribe((data) => {
      this.updateTime = data.updateTime;
    });
  }

  selectedSitesChange(selectedSites) {
    console.info('selectedSites: ', selectedSites);
    let arr = this.sites.map((site) => {
      let isShowArticle = selectedSites.includes(site);
      return {
        site,
        isShowArticle,
      };
    });

    this.siteService.saveLocalSites(arr);
  }

  reload() {
    this.articleService.scrollSubject.next(true);
  }

  taskUpdate() {
    this.siteService.taskUpdate().subscribe((data) => {
      this.snackBar.open(`${data.start ? '开始采集' : '采集失败'}`, 'ok', {
        duration: 2000,
      });
    });
  }
}
