import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';

import { Site, SiteService } from '../services/site.service';
import { tap, flatMap, catchError, map } from 'rxjs/operators';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'app-site-list',
  templateUrl: './site-list.component.html',
  styleUrls: ['./site-list.component.scss'],
})
export class SiteListComponent implements OnInit {
  dataSource: MatTableDataSource<Site>;
  displayedColumns: string[] = ['site', 'description', 'createdAt'];
  exportUrl: string;
  sitesImport: string;

  isShowImport: boolean = false;

  constructor(private siteService: SiteService) {}

  ngOnInit() {
    this.siteService
      .getSites()
      .pipe(
        tap((sites: Site[]) => {
          this.dataSource = new MatTableDataSource(sites);
        }),
        flatMap((sites) => {
          let siteLatestCrawlerTimeList = sites.map((item) => {
            return this.siteService.latestCrawlerTime(item.site).pipe(
              map((v)=> {
                if(!v) {
                  return {...item, updatedAt: new Date(0)};
                }
                return v;
              }),
              catchError((e) => {
                console.warn(e);
                return {...item, updatedAt: new Date(0)} as any;
              })
            )
          });

          return forkJoin(...siteLatestCrawlerTimeList);
        }),
        catchError((e, caught) => {
          console.warn(e);
          return caught;
        })
      )
      .subscribe((latestCrawlerTimeList) => {
        let keyBySite = latestCrawlerTimeList.reduce((result, current) => {
          result[current.site] = current;
          return result;
        }, {});

        this.dataSource.data.forEach((item) => {
          let latestCrawlerTime = keyBySite[item.site] || {};
          item.latestCrawlerTime = latestCrawlerTime.updatedAt;
        });

        console.info('this.dataSource: ', this.dataSource);
        console.info(this.dataSource.data);
      });

    this.exportUrl = this.siteService.getExportUrl();
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  toggle() {
    this.isShowImport = !this.isShowImport;
  }

  importSites(): void {
    let obj = JSON.parse(this.sitesImport);

    this.siteService.import(obj).subscribe(() => {
      location.reload();
    });
  }
}
